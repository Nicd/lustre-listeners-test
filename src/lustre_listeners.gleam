import gleam/int
import lustre
import lustre/attribute
import lustre/element.{text}
import lustre/element/html.{button, div, p}
import lustre/event.{on}

pub fn main() {
  let app = lustre.simple(init, update, view)
  let assert Ok(_) = lustre.start(app, "[data-lustre-app]", Nil)

  start_interval()

  Nil
}

fn init(_) {
  0
}

type Msg {
  Incr
  Decr
}

fn update(model, msg) {
  case msg {
    Incr -> model + 1
    Decr -> model - 1
  }
}

fn view(model) {
  let count = int.to_string(model)

  div([], [
    button([on("click", on_decr)], [text(" - ")]),
    p([], [text(count)]),
    button([attribute.id("plus"), on("click", on_incr)], [text(" + ")]),
  ])
}

fn on_decr(_) {
  Ok(Decr)
}

fn on_incr(_) {
  Ok(Incr)
}

@external(javascript, "./timer_ffi.mjs", "startInterval")
fn start_interval() -> Nil

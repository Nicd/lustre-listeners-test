# lustre_listeners

[![Package Version](https://img.shields.io/hexpm/v/lustre_listeners)](https://hex.pm/packages/lustre_listeners)
[![Hex Docs](https://img.shields.io/badge/hex-docs-ffaff3)](https://hexdocs.pm/lustre_listeners/)

```sh
gleam add lustre_listeners
```
```gleam
import lustre_listeners

pub fn main() {
  // TODO: An example of the project in use
}
```

Further documentation can be found at <https://hexdocs.pm/lustre_listeners>.

## Development

```sh
gleam run   # Run the project
gleam test  # Run the tests
gleam shell # Run an Erlang shell
```
